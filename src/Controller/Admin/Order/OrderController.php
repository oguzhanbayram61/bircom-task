<?php


namespace App\Controller\Admin\Order;


use App\Controller\Admin\AdminBaseController;
use App\Repository\OrderRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BasketController
 * @package App\Controller\Web\Basket
 * @Route("/order")
 */
class OrderController extends AdminBaseController
{

    /**
     * @Route("/list",name="admin_order_list")
     * @param OrderRepository $orderRepository
     * @return null |null
     */
    public function list(OrderRepository $orderRepository)
    {
        return $this->render('admin/order/list.html.twig', [
            'orders' => $orderRepository->lazyFindAll()
        ]);
    }

    /**
     * @Route("/detail/{order_no}",name="admin_order_detail")
     * @param $order_no
     * @param OrderRepository $orderRepository
     * @return null |null
     */
    public function detail($order_no, OrderRepository $orderRepository)
    {

        if (!$order = $orderRepository->findOneBy(['order_no' => $order_no])) {
            $this->addFlash('error', 'Böyle bir sipariş bulunmamaktadır.');
            return $this->redirectToRoute('admin_order_list');
        }

        return $this->render('admin/order/detail.html.twig', [
            'order' => $order
        ]);
    }

    /**
     * @Route("/approve/{order_no}",name="admin_order_approve")
     * @Route("/reject/{order_no}",name="admin_order_reject")
     * @param $order_no
     * @param Request $request
     * @param OrderRepository $orderRepository
     * @return null |null
     */
    public function orderApproveAndReject($order_no, Request $request, OrderRepository $orderRepository)
    {
        $action = $request->attributes->get('_route');

        if (!$order = $orderRepository->findOneBy(['order_no' => $order_no, 'status' => null])) {
            $this->addFlash('error', 'Böyle bir sipariş bulunamadı');
            return $this->redirectToRoute('admin_order_list');
        }

        switch ($action) {
            case 'admin_order_approve':
                $order->setStatus(true);
                foreach ($order->getOrderProducts() as $orderProduct) {
                    if ($orderProduct->getProduct()->getStock() - $orderProduct->getPiece() < 0) {
                        $this->addFlash('error', $orderProduct->getProduct()->getName() . ' isimli üründe yeterli stok yok lütfen stoklarınızı güncelleyiniz.');
                        return $this->redirectToRoute('admin_order_list');
                    } else {
                        $orderProduct->getProduct()->setStock($orderProduct->getProduct()->getStock() - $orderProduct->getPiece());
                    }
                }
                $this->addFlash('success', 'Sipariş Onaylandı');
                break;
            case 'admin_order_reject':
                $order->setStatus(false);
                $this->addFlash('error', 'Sipariş Onaylanmadı');
                break;
            default:
                $this->addFlash('error', 'Bir sorun oluştu');
                break;
        }

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_order_list');

    }
}