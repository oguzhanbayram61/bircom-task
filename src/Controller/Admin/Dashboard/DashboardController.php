<?php


namespace App\Controller\Admin\Dashboard;


use App\Controller\Admin\AdminBaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AdminBaseController
{

    /**
     * @Route("/",name="admin_home")
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard/index.html.twig');
    }
}