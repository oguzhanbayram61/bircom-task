<?php


namespace App\Controller\Web\Product;


use App\Controller\Web\WebBaseController;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 * @package App\Controller\Web\Product
 * @Route("/product")
 */
class ProductController extends WebBaseController
{

    /**
     * @Route("/",name="web_product_list")
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function index(ProductRepository $productRepository): Response
    {
        return $this->render('web/product/index.html.twig', [
            'products' => $productRepository->findBy(['status' => true])
        ]);
    }

    /**
     * @Route("/{slug}",name="web_product_show")
     * @param $slug
     * @return Response
     */
    public function show($slug): Response
    {
        if (!$product = $this->getDoctrine()->getRepository(Product::class)->findOneBy(['slug' => $slug]))
            return $this->redirectToRoute('web_home');

        return $this->render('web/product/show.html.twig', [
            'product' => $product
        ]);
    }

}