<?php


namespace App\Controller\Web\Home;


use App\Controller\Web\WebBaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends WebBaseController
{

    /**
     * @Route("/",name="web_home")
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('web/home/index.html.twig');
    }
}