<?php


namespace App\Controller\Web\Order;


use App\Controller\Web\WebBaseController;
use App\Entity\Basket;
use App\Entity\Order;
use App\Entity\User;
use App\Entity\UserAddress;
use App\Repository\OrderProductRepository;
use App\Repository\OrderRepository;
use App\Repository\UserAddressRepository;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BasketController
 * @package App\Controller\Web\Basket
 * @Route("/order")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class OrderController extends WebBaseController
{
    /**
     * @Route("/checkout",name="web_order_checkout")
     * @return Response
     */
    public function index()
    {
        if (!$baskets = $this->getDoctrine()->getRepository(Basket::class)->findBy(['user' => $this->getUser()]))
            return $this->redirectToRoute('web_basket_show');

        return $this->render('web/order/index.html.twig', [
            'baskets' => $baskets
        ]);
    }

    /**
     * @Route("/new",name="web_order_new")
     * @param Request $request
     * @param UserAddressRepository $userAddressRepository
     * @param OrderProductRepository $orderProductRepository
     * @return Response
     */
    public function new(Request $request, UserAddressRepository $userAddressRepository, OrderProductRepository $orderProductRepository)
    {
        try {
            /** @var User $user */
            $user = $this->getUser();

            $em = $this->getDoctrine()->getManager();

            $address = $userAddressRepository->createAddress($user, $request->request->all());

            $order = new Order();
            $order->setUser($user);
            $order->setAddress($address);
            $order->setOrderNo(md5(uniqid()));
            $order->setPaymentType($request->request->get('payment_type'));
            $em->persist($order);

            if (!$orderProduct = $orderProductRepository->create($user->getBaskets(), $order))
                throw new Exception('Bir sorun oluştu,lütfen tekrar deneyin');

            $em->flush();

            $this->addFlash('success', 'Siparişiniz alınmıştır.');
            return $this->redirectToRoute('web_order_list');

        } catch (Exception $e) {
            $this->addFlash('error', $e->getPrevious() ? $e->getPrevious()->getMessage() : $e->getMessage());
            return $this->redirectToRoute('web_order_checkout');
        }
    }

    /**
     * @Route("/list",name="web_order_list")
     * @param OrderRepository $orderRepository
     * @return null |null
     */
    public function list(OrderRepository $orderRepository)
    {
        return $this->render('web/order/list.html.twig', [
            'orders' => $orderRepository->lazyFindAll($this->getUser())
        ]);
    }

    /**
     * @Route("/detail/{order_no}",name="web_order_detail")
     * @param $order_no
     * @param OrderRepository $orderRepository
     * @return null |null
     */
    public function detail($order_no, OrderRepository $orderRepository)
    {

        if (!$order = $orderRepository->findOneBy(['user' => $this->getUser(), 'order_no' => $order_no]))
            return $this->redirectToRoute('web_home');

        return $this->render('web/order/detail.html.twig', [
            'order' => $order
        ]);
    }
}