<?php


namespace App\Controller\Web\Basket;


use App\Controller\Web\WebBaseController;
use App\Entity\Basket;
use App\Entity\Product;
use App\Repository\BasketRepository;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BasketController
 * @package App\Controller\Web\Basket
 * @Route("/basket")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class BasketController extends WebBaseController
{

    /**
     * @Route("/add",name="web_add_basket")
     * @param Request $request
     * @param BasketRepository $basketRepository
     * @return JsonResponse
     */
    public function new(Request $request, BasketRepository $basketRepository): JsonResponse
    {
        try {
            if ($request->request->getInt('piece') < 1)
                throw new Exception('En az 1 ürün eklemelisiniz');

            if (!$user = $this->getUser())
                throw new Exception('Lütfen önce giriş yapınız');

            $em = $this->getDoctrine()->getManager();

            if (!$product = $em->getRepository(Product::class)->findOneBy(['id' => $request->request->getInt('product_id'), 'status' => true]))
                throw new Exception('Böyle bir ürün bulunmuyor');

            if ($basket = $basketRepository->findOneBy(['user' => $user, 'product' => $product]))
                $basketRepository->basketPieceUpdate($basket->getId(), $basket->getPiece() + $request->request->getInt('piece'));
            else
                $basketRepository->basketCreate($user, $product, $request->request->all());

            return $this->json([
                'status' => true,
                'message' => 'Ürün Sepete Eklendi',
                'basket_count' => $basketRepository->basketPieceTotal($user)
            ]);

        } catch (Exception $e) {

            return $this->json([
                'status' => false,
                'message' => $e->getPrevious() ? $e->getPrevious()->getMessage() : $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/piece-update",name="web_basket_piece_update")
     * @param Request $request
     * @param BasketRepository $basketRepository
     * @return JsonResponse
     */
    public function basketPieceUpdate(Request $request, BasketRepository $basketRepository): JsonResponse
    {
        try {
            if ($request->request->getInt('piece') < 1)
                throw new Exception('En az 1 ürün eklemelisiniz');

            $em = $this->getDoctrine()->getManager();

            if (!$product = $em->getRepository(Product::class)->findOneBy(['id' => $request->request->getInt('product_id'), 'status' => true]))
                throw new Exception('Böyle bir ürün bulunmuyor');

            if ($basket = $basketRepository->findOneBy(['user' => $this->getUser(), 'product' => $product]))
                $basketRepository->basketPieceUpdate($basket->getId(), $request->request->getInt('piece'));

            return $this->json([
                'status' => true,
                'message' => 'Ürün Adeti Güncellendi',
            ]);

        } catch (Exception $e) {

            return $this->json([
                'status' => false,
                'message' => $e->getPrevious() ? $e->getPrevious()->getMessage() : $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/basket-piece-total",name="web_basket_piece_total")
     * @param BasketRepository $basketRepository
     * @return RedirectResponse|Response
     */
    public function basketPieceTotal(BasketRepository $basketRepository): Response
    {
        if (!$user = $this->getUser())
            return $this->redirectToRoute('app_login');

        return $this->render('web/basket/basket_button.html.twig', [
            'basket_count' => $basketRepository->basketPieceTotal($user)
        ]);
    }

    /**
     * @Route("/show",name="web_basket_show")
     */
    public function show(): Response
    {
        $baskets = $this->getDoctrine()->getRepository(Basket::class)->findBy(['user' => $this->getUser()]);
        return $this->render('web/basket/show.html.twig', [
            'baskets' => $baskets
        ]);
    }

    /**
     * @Route("/delete",name="web_basket_delete")
     * @param Request $request
     * @param BasketRepository $basketRepository
     * @return JsonResponse
     */
    public function delete(Request $request, BasketRepository $basketRepository): JsonResponse
    {
        try {

            $em = $this->getDoctrine()->getManager();

            if (!$basket = $em->getRepository(Basket::class)->find($request->request->getInt('basket_id')))
                throw new Exception('Böyle bir ürün bulunmuyor');

            $em->remove($basket);
            $em->flush();

            return $this->json([
                'status' => true,
                'message' => 'Ürün Silindi',
            ]);

        } catch (Exception $e) {

            return $this->json([
                'status' => false,
                'message' => $e->getPrevious() ? $e->getPrevious()->getMessage() : $e->getMessage()
            ]);
        }
    }

}