<?php

namespace App\Form;

use App\Entity\Brand;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('brand', EntityType::class, [
                'class' => Brand::class,
                'label' => 'Marka',
                'choice_label' => 'name',
                'placeholder' => 'Seç',
            ])
            ->add('name', TextType::class, [
                'label' => 'Ad'
            ])
            ->add('code', TextType::class, [
                'label' => 'Kod'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Açıklama',
                'attr' => [
                    'class' => 'ckeditor'
                ]
            ])
            ->add('price', MoneyType::class, [
                'label' => 'Birim Fiyat',
                'currency' => 'TRY'
            ])
            ->add('stock', IntegerType::class, [
                'label' => 'Stok'
            ])
            ->add('status', ChoiceType::class, [
                'label' => 'Durumu',
                'choices' => [
                    'Aktif' => true,
                    'Pasif' => false
                ]
            ])
            ->add('image', FileType::class, [
                'label' => 'Ürün Resmi',
                'constraints' => [
                    new File([
                        'maxSize' => '4096k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Lütfen geçerli bir resim yükleyiniz.',
                    ])
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
