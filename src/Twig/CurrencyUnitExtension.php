<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class CurrencyUnitExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('currency_unit', [$this, 'currencyUnit']),
        ];
    }

    public function currencyUnit()
    {
        return 'TL';
    }
}
