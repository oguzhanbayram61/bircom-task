<?php

namespace App\Repository;

use App\Entity\Basket;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Basket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Basket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Basket[]    findAll()
 * @method Basket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BasketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Basket::class);
    }

    public function basketCreate(User $user, Product $product, $data)
    {
        try {

            $basket = new Basket();
            $basket->setUser($user);
            $basket->setProduct($product);
            $basket->setPiece((int)$data['piece']);
            $this->_em->persist($basket);
            $this->_em->flush();

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }


    public function basketPieceTotal(User $user)
    {
        return $this->createQueryBuilder('b')
            ->select('SUM(b.piece)')
            ->where('b.user = :user')->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();

    }

    public function basketPieceUpdate($id, $piece)
    {
        return $this->createQueryBuilder('b')
            ->update()
            ->set('b.piece', '?1')
            ->setParameter(1, $piece)
            ->where('b.id = :id')->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
