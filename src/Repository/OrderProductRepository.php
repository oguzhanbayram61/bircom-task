<?php

namespace App\Repository;

use App\Entity\Basket;
use App\Entity\Order;
use App\Entity\OrderProduct;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderProduct[]    findAll()
 * @method OrderProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderProduct::class);
    }

    public function create($baskets, Order $order)
    {
        try {
            /** @var Basket $basket */
            foreach ($baskets as $basket) {
                $orderProduct = new OrderProduct();
                $orderProduct->setProduct($basket->getProduct());
                $orderProduct->setPiece($basket->getPiece());
                $orderProduct->setPrice($basket->getProduct()->getPrice() * $basket->getPiece());
                $orderProduct->setOrder($order);
                $this->_em->persist($orderProduct);
                $this->_em->remove($basket);
            }
            return true;
        } catch (ORMException $e) {
            return false;

        }
    }

}
