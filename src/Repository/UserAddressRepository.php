<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\UserAddress;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserAddress|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserAddress|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserAddress[]    findAll()
 * @method UserAddress[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserAddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserAddress::class);
    }

    /**
     * @param User $user
     * @param $data
     * @return UserAddress|bool
     */
    public function createAddress(User $user, $data)
    {
        try {
            $address = new UserAddress();
            $address->setUser($user);
            $address->setName($data['name']);
            $address->setSurname($data['surname']);
            $address->setCity($data['city']);
            $address->setDistrict($data['district']);
            $address->setAddress($data['address']);
            $address->setAddressName($data['address_name']);
            $address->setPhone($data['phone']);

            $this->_em->persist($address);
            return $address;

        } catch (ORMException $e) {
            return false;
        }
    }

}
