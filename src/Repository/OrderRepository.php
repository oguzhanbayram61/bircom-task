<?php

namespace App\Repository;

use App\Entity\Order;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }


    public function lazyFindAll(User $user = null)
    {
        $query = $this->createQueryBuilder('o')
            ->innerJoin('o.orderProducts', 'op')
            ->innerJoin('o.user', 'u')
            ->select('o.order_no', 'o.status', 'o.createdAt', 'o.payment_type')
            ->addSelect('u.id AS user_id', 'u.name', 'u.surname')
            ->addSelect('SUM(op.piece) AS piece')
            ->addSelect('SUM(op.price) AS total_price')
            ->groupBy('o.id');

        if ($user)
            $query->where('o.user = :user')->setParameter('user', $user);

        return $query->getQuery()->getResult();
    }

}
