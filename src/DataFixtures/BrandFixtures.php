<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use Cocur\Slugify\SlugifyInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BrandFixtures extends Fixture
{
    private $slugger;

    public function __construct(SlugifyInterface $slugify)
    {
        $this->slugger = $slugify;
    }

    public function load(ObjectManager $manager)
    {
        foreach ($this->getBrandData() as [$name]) {
            $brand = new Brand();
            $brand->setName($name);
            $brand->setSlug($this->slugger->slugify($name));
            $manager->persist($brand);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    private function getBrandData(): array
    {
        return [

            ['Asus'],
            ['Apple'],
            ['Lenovo'],
            ['Msi'],
            ['Acer'],
            ['Hp'],
            ['Dell'],
            ['Samsung'],

        ];
    }
}
