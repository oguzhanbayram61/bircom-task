# Bircom

## Gereklilikler :

- PHP `>=7.1`.
- Composer

### Kurulum

- #### 1.Adım
    `.env` dosyasındaki `db_user`,`db_password`,`db_name` kendi veritabanı bilgilerimizi yazalım. 
    ###
    ```sh
    DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
    ```

- #### 2.Adım
    ```sh
    composer install
    ```

- #### 3.Adım
    ```sh
    php bin/console doctrine:database:create
    ```
  
- #### 4.Adım
    ```sh
    php bin/console doctrine:schema:update --force
    ```
  
- #### 5.Adım
    ```sh
    php bin/console doctrine:fixtures:load --append
    ```
  
 - #### 6.Adım
     ```sh
     php bin/console server:run
     ```
   
+  Kullanıcı Adı : bircom@admin.com
+  Şifre : password